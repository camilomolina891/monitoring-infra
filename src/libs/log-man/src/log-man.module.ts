import { Module } from '@nestjs/common';
import { LogManService } from './log-man.service';

@Module({
  providers: [LogManService],
  exports: [LogManService],
})
export class LogManModule {}
